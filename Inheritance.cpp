
#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "some voice" << std::endl;
    }

};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Bark, Bark!" << std::endl;
    }

};

class Cat : public Animal
{    
public:
    void Voice() override
    {
        std::cout << "Meow, Meow!" << std::endl;
    }

};

class Pig : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Oink, Oink!" << std::endl;
    }

};


int main()
{
    int ArraySize = 3;
    Animal** animal = new Animal * [ArraySize]{ new Dog(), new Cat(), new Pig() };

    for (int i = 0; i < ArraySize; i++)
    {
        animal[i]->Voice();
    }

    delete[] animal;

}
